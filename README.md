This script posts from Enco DAD to Dubbletrack.

A cut should be set up in DAD that gets triggered after a song plays. That cut should look like this:

```
  delay next md 1000
  generate ASPLAY STUDIO1@M@D@Y [k]\dubbletrack-remote\playlists\20@Y-@M-@D-automation.tsv
  delay next cmd 2000
  system 'ruby k:\dubbletrack\dubbletrack-remote.rb update'
```

settings-example.yml should be copied to settings.yml and modified with your settings

To install and run on windows `bundle install --without development test`
