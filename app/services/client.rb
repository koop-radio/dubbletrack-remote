# frozen_string_literal: true

module DubbleTrackRemote
  class Client
    def initialize(
      url: Settings.url,
      key: Settings.key,
      secret: Settings.secret
    )
      @client = Faraday.new(url: url) do |faraday|
        faraday.request :retry, 
          max: 2,
          interval: 0.05, 
          interval_randomness: 0.5, 
          backoff_factor: 2

        faraday.response :logger do | logger |
          logger.filter(/(X-api-key\:)(\w+)/,'\1[REMOVED]')
          logger.filter(/(X-api-secret\:)(\w+)/,'\1[REMOVED]')
        end

        faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
      end

      @url    = url # ||'https://dubbletrack.com/api/v1/stations/koop/shows/koop-automation/tracks'
      @key    = key # || 'dt_xxxxxxxx'
      @secret = secret # || 'dts_xxxxxxxxxxxx'
    end

    def send(items)
      items = [items].flatten

      payload = JSON.generate(
        data: items.collect { |item| item_json(item) }
      )

      headers = {
        "Content-Type": 'application/json',
        "X-API-KEY": @key,
        "X-API-SECRET": @secret,
        'User-Agent' => 'Dubbletrack Remote'
      }

      items.each { |i| i.last_attempt_at = Time.now.iso8601 }
      begin
        response = @client.post(@url, payload, headers)
        if response.success?
          begin
            items.each do |i|
              i.success = true
              i.save
            end
            # item.remote_id = JSON.parse(response.body)['data']['id']
          rescue Exception => e
            puts e
          end
        else
          items.each do |item| 
            item.success   = false
            error = DubbleTrackRemote::Error.new(response)
            raise DubbleTrackRemote::RateLimitError if error.text == 'Rate Limit Exceeded'
            item.last_error_text = error.text
            item.last_error_code = error.code
            item.save
          end
        end
      rescue Faraday::ConnectionFailed => exception
        items.each do |item| 
          item.last_error_text = 'connection failed'
          item.last_error_code = 404
          item.save
        end
      end
    end

    private

    def item_json(item)
      {
        type: item.item_type,
        attributes: {
          title: item.title,
          artist_name: item.artist_name,
          release_name: item.release_name,
          label_name: item.label_name,
          played_at: item.played_at
        }.filter { |k, v| v.present? }
      }
    end
  end
end
