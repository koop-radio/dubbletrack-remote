
# Reads in a TSV file and creates items out of them
module DubbleTrackRemote
  class FileReader
    def initialize(path)
      @tsv_path = path # ENV['ENCO_LOG_PATH'] || 'RA071819.CSV' ||"K:\\DAD\\ASPLAY\\"
    end

    def items
      @items ||= read_items.select.sort_by(&:played_at)
    end

    def tracks
      items.select { |i| i.item_type == DubbleTrackRemote::Item::TRACK }
    end

    def traffic
      items.select { |i| i.item_type == DubbleTrackRemote::Item::TRAFFIC }
    end

    def ingest
      items.each do |item|
        if item.changed?
          puts item.to_json
          item.save
        end
      end
    end

    protected

    def header
      @header ||= entries.slice(0, 1).flatten
    end

    def header_indexes
      @header_indexes ||= {}.tap do |hi|
        header.flatten.each_with_index do |value, index|
          hi[value] = index
        end
      end
    end

    def timestamp(date, time)
      month, day, year = date.split('/')
      Time.parse("20#{year}-#{month}-#{day} #{time}")
    end
    
    def rows
      @rows ||= entries.slice(1...)
    end

    def parse_line(line)
      parsed = CSV.parse(line.to_s, liberal_parsing: true, col_sep: "\t", headers: false).try(:first)
      if parsed.flatten.size > 0 && parsed.any? { |p| p.present? }
        parsed.flatten.collect { |v| v.strip }
      else
        nil
      end
    end

    def entries
      @entries ||= file_lines.collect do |line|
        parse_line(line) unless line.to_s.strip.blank?
      end.compact
    end

    def file_lines
      @file_lines ||= begin
        file = File.read(@tsv_path)
        cd = CharDet.detect(file)
        file = File.read(@tsv_path, encoding: cd['encoding'])

        lines = file.lines.collect do |line|
          line.mb_chars.tidy_bytes.to_s
        end.select { |l|
          l.to_s.strip.present?
        }
      rescue
        lines = [] unless lines.present?
      end
    end

    def entry_to_hash(entry)
      label_to_index = header_indexes
      index_to_label = header_indexes.invert

      entry_to_hash = {}.tap do |hash|
        entry.each_with_index do |value, index|
          hash[index_to_label[index]] = value
        end
      end
    end

    def read_items
      items = []

      rows.each do |entry|
        hash = entry_to_hash(entry)

        next if hash.all? { |h,v| h.strip == v.strip }

        title   = (hash['TITLE'] || '').strip
        artist  = (hash['ARTIST'] || '').strip
        album   = (hash['ALBUM'] || '').strip
        label   = (hash['LABEL'] || '').strip
        date    = (hash['DATE'] || '').strip
        time    = (hash['TIME'] || '').strip

        item = Item.find_or_initialize_by(played_at: timestamp(date, time))
        if (item.new_record?)
          item.title = title
          item.label_name = label
          item.artist_name = artist
          item.release_name = album
        end
        item.valid? #run validations, set item_type

        items << item
      end

      items
    end
  end
end
