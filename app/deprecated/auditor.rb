# frozen_string_literal: true

require 'json'

module DubbleTrackRemote
  class Auditor
    attr_reader :file_path

    def initialize(data_file_path)
      @file_path = log_file_path(data_file_path)
      read_keys
    end

    def read_keys
      @keys = begin
                file = File.read(@file_path)
                JSON.parse(file) || {}
              rescue StandardError
                {}
              end
    end

    def remote_id_for_key(key)
      @keys ||= {}
      @keys['success'] ||= {}
      record = @keys['success'][key]
      record['remote_id'] if record
    end

    def errors_for_key(key)
      @keys ||= {}
      @keys['failures'] ||= {}
      record = @keys['failures'][key]
      record['remote_errors'] if record
    end

    private

    def log_file_path(file_path)
      dir = File.dirname(file_path)
      name = File.basename(file_path).gsub(File.extname(file_path), '-log.json')

      File.join(dir, name)
    end
  end
end
