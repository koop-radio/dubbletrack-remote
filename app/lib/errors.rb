# frozen_string_literal: true

module DubbleTrackRemote
  class RateLimitError
  end

  class Error
    attr_accessor :time, :text, :code

    def initialize(response)
      @time = Time.now.iso8601
      @code = response.status
      @text = error_text(response)
    end

    def error_text(response)
      error_text = response.body
      begin
        json = JSON.parse(response.body)
        errors = json['errors'] || []
        error_text = errors.collect { |error| error['detail'] }.join(',')
      rescue JSON::ParserError
        # keep the error text as response.body
      end
      return error_text
    end

    def to_json
      {
        text: @text,
        code: @code,
        time: @time
      }
    end
  end
end
