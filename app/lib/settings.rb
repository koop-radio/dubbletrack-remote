require 'settingslogic'

module DubbleTrackRemote
  class Settings < Settingslogic
    source File.expand_path('config/settings.yml')
    namespace ENV['DUBBLETRACK_REMOTE_ENV'] || 'production'
    load!
  end
end
