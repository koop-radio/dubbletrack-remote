# An item to be sent to dubbletrack. Either a track or a traffic update, can be created from a hash or a db record
# module DubbleTrackRemote
module DubbleTrackRemote
  class Item < ActiveRecord::Base
    TRACK = 'track'
    TRAFFIC = 'traffic'

    scope :successful, -> { where(success: true).order(:played_at, :desc) }
    scope :remaining, -> { where(success: false) }
    scope :tracks, -> { where(['item_type = ?', TRACK]) }
    scope :traffic, -> { where(['item_type = ?', TRAFFIC]) }
    scope :recent_remaining, -> { remaining.where(['played_at > ?', 1.day.ago]) }
    scope :next, ->(count = 10) { remaining.where(['last_attempt_at < ? OR last_attempt_at IS NULL', 30.minutes.ago]).order('played_at DESC').limit(count) }
    scope :on_date,   ->(time)   { where(['played_at >= ? AND played_at <= ?', time.beginning_of_day, time.end_of_day]) }

    before_validation :set_item_type
    before_save :set_item_type

    def set_item_type
      self.item_type = detect_item_type
    end

    def detect_item_type
      return TRACK if track?
      return TRAFFIC if traffic?
    end

    def key
      "#{played_at.iso8601}-#{title}:#{artist_name}"
    end

    def track?
      title.present? &&  played_at.present? && artist_name.present? # label name and release name could technically be left out, even though they shouldn't
    end

    def traffic?
      title && played_at.present? && !track?
    end
  end
end