require 'bundler'
Bundler.require
require_all 'app'
require "sinatra/activerecord"
Sinatra::Application.set :database, DubbleTrackRemote::Settings.database