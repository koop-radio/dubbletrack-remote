# frozen_string_literal: true

require_relative 'config/environment'

module DubbleTrackRemote
  class CLI < Thor
    desc 'watch', 'watch folder and post on changes'
    def watch
      listener = Listen.to(playlist_directory_path, only: /.tsv$/i) do |modified, added, removed|
        if modified
          modified.each { |m| FileReader.new(m).ingest }
        elsif added
          added.each { |a| FileReader.new(a).ingest }
        end
      end

      listener.start

      Thread.new do
        loop do
          @client = Client.new
          @client.send(Item.next(10))
          status
          sleep 10
        end
      end

      sleep
    end

    desc 'migrate', 'upgrade to db'
    def migrate
      Dir.glob("#{playlist_directory_path}/*.TSV").sort.each do |file_path|
        auditor  = Auditor.new(file_path)
        reader   = FileReader.new(file_path)
        puts "processing #{file_path}"
        reader.items.each do |item|
          item.remote_id = auditor.remote_id_for_key(item.key)
          item.success   = !!item.remote_id
          item.save
        end
      end
    end

    desc 'status', 'get status'
    def status
      puts "total remaining: #{Item.remaining.count}"
      puts "total transmitted: #{Item.successful.count}"
      puts "recent items needing transmission: #{Item.recent_remaining.count}"
      puts "last successful transmit: #{Item.successful.last.try(:played_at)}"
    end

    desc 'update', 'post latest tracks from current playlist'
    def update
      status

      if File.exist?(todays_playlist_path)
        FileReader.new(todays_playlist_path).ingest
      end

      @client = Client.new
      @client.send(Item.next(10))
    end

    desc 'post FILE_PATH', 'post latest track from playlist'
    def post(file_path)
      FileReader.new(file_path).ingest
      WorkQueue.new.update
    end

    desc 'read FILE_PATH', 'read tracks from playlist'
    def read(file_path)
      FileReader.new(file_path).items.each { |e|
        puts e.to_json
      }
    end

    protected

    def playlist_directory_path
      File.expand_path(File.join(File.dirname(__FILE__), local_playlist_directory_name))
    end

    def todays_playlist_path
      File.join(playlist_directory_path, "#{Time.now.strftime('%Y-%m-%d')}.tsv")
    end

    def local_playlist_directory_name
      "playlists"
    end

  end
end

DubbleTrackRemote::CLI.start(ARGV)
