`{define_begin}` to `{define_end}` defines variables to be referenced later.

`{relation_begin}` to `{relation_end}` is essential, and I think it sets up where this data is being pulled from

`{pagesize}` dictates how many items are printed before that `{page_header_begin}` to `{header_end}` is printed again. This caused the old "csv" report to be broken up with sections of non-csv data.

the `{header_begin}` to `{header_end}` section at the bottom is what gets printed for each entry.

The `{if} d9 == "4 "` statement is checking that only tracks that come from deck #4 get logged.

Each column of the file (track, artist, etc) is dictated by the bracketed sections, where the number of spaces after the referenced variable equates to the maximum width to allow for that value before it gets chopped (so dumb, I know).  `[d2                                   ]`

The format we want to output is a tab separated file, so we when there are commas in a track it doesn't cause problems. We also want to make the field widths as large as possible to prevent truncation, and the page size huge to prevent that silly page header from printing out periodically.

Puts this new Asplay.rpg file in K:\Dad\Files\Asplay.RPG
