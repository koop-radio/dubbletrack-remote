class Schema < ActiveRecord::Migration[6.0]
  def change
    create_table :items, force: true do |t|
      t.datetime :played_at, index: true, unique: true
      t.string :item_type
      t.string :title
      t.string :artist_name
      t.string :label_name
      t.string :release_name
      t.boolean :success
      t.string :remote_id
      t.datetime :last_attempt_at
      t.string :last_error_text
      t.integer :last_error_code

      t.timestamps
    end
  end
end
