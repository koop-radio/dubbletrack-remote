# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 1) do

  create_table "items", force: :cascade do |t|
    t.datetime "played_at"
    t.string "item_type"
    t.string "title"
    t.string "artist_name"
    t.string "label_name"
    t.string "release_name"
    t.boolean "success"
    t.string "remote_id"
    t.datetime "last_attempt_at"
    t.string "last_error_text"
    t.integer "last_error_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["played_at"], name: "index_items_on_played_at"
  end

end
