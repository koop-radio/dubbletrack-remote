# frozen_string_literal: true
require_relative '../spec_helper'

RSpec.describe DubbleTrackRemote::FileReader, type: :model do
  it 'reads the example file correctly' do
    path   = File.expand_path(File.join(File.dirname(__FILE__), "../../example/2019-10-31.tsv"))
    reader = DubbleTrackRemote::FileReader.new(path)
    expect(reader.tracks.size).to eq(167)
    expect(reader.tracks.last.artist_name).to eq("Hollywood Gossip"), "sort is correct"

    expect(reader.traffic.size).to eq(57)
  end

  it 'it gets the the right unsent items' do
    path   = File.expand_path(File.join(File.dirname(__FILE__), "../../example/2019-10-31.tsv"))
    reader = DubbleTrackRemote::FileReader.new(path)

    expect(reader.tracks.last.title).to eq("Summer Haze"), "last unsent track is correct"
  end

  it 'does not include headers' do
    path   = File.expand_path(File.join(File.dirname(__FILE__), "../../example/2019-10-31.tsv"))
    reader = DubbleTrackRemote::FileReader.new(path)
    headers = reader.items.select { |item| item.title == 'TITLE' && item.artist_name == 'ARTIST' }

    expect(headers.size).to eq(0)
  end

end
