# # frozen_string_literal: true

# require_relative '../spec_helper'

# RSpec.describe DubbleTrackRemote::WorkQueue, type: :model do
#   before do
#     ENV['DUBBLETRACK_URL'] = 'http://api.dubbletrack.test/api/v1/endpoint'
#     ENV['DUBBLETRACK_KEY'] = '_key_'
#     ENV['DUBBLETRACK_SECRET'] = '_secret_'
#   end

#   it 'posts the two waiting tracks' do
#     path = File.expand_path(File.join(File.dirname(__FILE__), '../../example/2019-10-31.tsv'))

#     DubbleTrackRemote::Item.any_instance.should_receive(:log_success).once
#     DubbleTrackRemote::Item.any_instance.should_receive(:log_failure).once

#     stub_request(:post, 'http://api.dubbletrack.test/api/v1/endpoint')
#       .with(
#         body: '{"data":{"type":"track","attributes":{"title":"Calico Train","artist_name":"Steve Martin","release_name":"The Crow (New Songs For The Five-String Banjo)","label_name":"Rounder","played_at":"2019-10-31 20:37:45 -0500"}}}',
#         headers: {
#           'Accept' => '*/*',
#           'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
#           'Content-Type' => 'application/json',
#           'User-Agent' => 'Faraday v1.0.0',
#           'X-Api-Key' => '_key_',
#           'X-Api-Secret' => '_secret_'
#         }
#       )
#       .to_return(status: 200, body: '{"data":{"id":"1"}}', headers: {})

#     stub_request(:post, 'http://api.dubbletrack.test/api/v1/endpoint')
#       .with(
#         body: "{\"data\":{\"type\":\"track\",\"attributes\":{\"title\":\"Summer Haze\",\"artist_name\":\"Hollywood Gossip\",\"release_name\":\"Good Danny's Good Friends Vol. 1\",\"label_name\":\"Self Released\",\"played_at\":\"2019-10-31 20:40:35 -0500\"}}}",
#         headers: {
#           'Accept' => '*/*',
#           'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
#           'Content-Type' => 'application/json',
#           'User-Agent' => 'Faraday v1.0.0',
#           'X-Api-Key' => '_key_',
#           'X-Api-Secret' => '_secret_'
#         }
#       )
#       .to_return(status: 412, body: '{"errors":["whatever"]}', headers: {})

#     runner = DubbleTrackRemote::WorkQueue.new(path)
#     expect(runner.reader.ready_items.size).to eq(2), "two tracks should be ready"

#     runner.update
#   end

#   it "tries posting tracks until three errors have been reached then gives up" do
#     path = File.expand_path(File.join(File.dirname(__FILE__), '../../example/2019-10-31.tsv'))

#     stub_request(:post, 'http://api.dubbletrack.test/api/v1/endpoint')
#       .with(
#         body: '{"data":{"type":"track","attributes":{"title":"Calico Train","artist_name":"Steve Martin","release_name":"The Crow (New Songs For The Five-String Banjo)","label_name":"Rounder","played_at":"2019-10-31 20:37:45 -0500"}}}',
#         headers: {
#           'Accept' => '*/*',
#           'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
#           'Content-Type' => 'application/json',
#           'User-Agent' => 'Faraday v1.0.0',
#           'X-Api-Key' => '_key_',
#           'X-Api-Secret' => '_secret_'
#         }
#       )
#       .to_return(status: 200, body: '{"data":{"id":"1"}}', headers: {})

#     stub_request(:post, 'http://api.dubbletrack.test/api/v1/endpoint')
#       .with(
#         body: "{\"data\":{\"type\":\"track\",\"attributes\":{\"title\":\"Summer Haze\",\"artist_name\":\"Hollywood Gossip\",\"release_name\":\"Good Danny's Good Friends Vol. 1\",\"label_name\":\"Self Released\",\"played_at\":\"2019-10-31 20:40:35 -0500\"}}}",
#         headers: {
#           'Accept' => '*/*',
#           'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
#           'Content-Type' => 'application/json',
#           'User-Agent' => 'Faraday v1.0.0',
#           'X-Api-Key' => '_key_',
#           'X-Api-Secret' => '_secret_'
#         }
#       )
#       .to_return(status: 412, body: '{"errors":["whatever"]}', headers: {})

#     work_queue = DubbleTrackRemote::WorkQueue.new(path)
#     expect(work_queue.todo.size).to eq(2)

#     work_queue.update

#     expect(work_queue.todo.size).to eq(1), 'after one is sent, one should remain'

#     work_queue.update

#     expect(work_queue.todo.size).to eq(1), "second try, one should still remain"


#     expect(work_queue.todo.size).to eq(1), "third try, one should still remain"

#     work_queue.update

#     expect(work_queue.todo.size).to eq(0), "three strikes, no more tries"
#   end
# end
