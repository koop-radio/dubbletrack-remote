# frozen_string_literal: true

require_relative '../spec_helper'

RSpec.describe DubbleTrackRemote::Client, type: :model do
  before do
    ENV['DUBBLETRACK_URL'] = 'http://api.dubbletrack.test/api/v1/endpoint'
    ENV['DUBBLETRACK_KEY'] = '_key_'
    ENV['DUBBLETRACK_SECRET'] = '_secret_'
  end

  it 'posts correctly' do
    client = DubbleTrackRemote::Client.new

    item = DubbleTrackRemote::Item.create(
      title: 'Fine For Now',
      artist_name: 'Grizzly Bear',
      release_name: 'Veckatimest',
      label_name: 'Warp Records',
      played_at: '2020-10-31 00:01:27'
    )

    stub_request(:post, "http://api.dubbletrack.test/api/v1/endpoint").
    with(
      body: "{\"data\":[{\"type\":\"track\",\"attributes\":{\"title\":\"Fine For Now\",\"artist_name\":\"Grizzly Bear\",\"release_name\":\"Veckatimest\",\"label_name\":\"Warp Records\",\"played_at\":\"2020-10-31 00:01:27 UTC\"}}]}",
      headers: {
      'Accept'=>'*/*',
      'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
      'Content-Type'=>'application/json',
      'User-Agent'=>'Dubbletrack Remote',
      'X-Api-Key'=>'_key_',
      'X-Api-Secret'=>'_secret_'
      }).
      to_return(status: 200, body: "", headers: {})
    client.send([item])
  end
end
