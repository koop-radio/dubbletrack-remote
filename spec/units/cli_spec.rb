# frozen_string_literal: true

require_relative '../spec_helper'

RSpec.describe DubbleTrackRemote::CLI, type: :model do
  before do
    ENV['DUBBLETRACK_URL'] = 'http://api.dubbletrack.test/api/v1/endpoint'
    ENV['DUBBLETRACK_KEY'] = '_key_'
    ENV['DUBBLETRACK_SECRET'] = '_secret_'
    Timecop.freeze("2019-10-31")
  end

  xit 'watches for changes' do
    
  end

  # it 'posts two tracks' do
  #   stub_request(:post, 'http://api.dubbletrack.test/api/v1/endpoint')
  #     .with(
  #       body: '{"data":{"type":"track","attributes":{"title":"Calico Train","artist_name":"Steve Martin","release_name":"The Crow (New Songs For The Five-String Banjo)","label_name":"Rounder","played_at":"2019-10-31 20:37:45 -0500"}}}',
  #       headers: {
  #         'Accept' => '*/*',
  #         'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
  #         'Content-Type' => 'application/json',
  #         'User-Agent' => 'Faraday v1.0.0',
  #         'X-Api-Key' => '_key_',
  #         'X-Api-Secret' => '_secret_'
  #       }
  #     )
  #     .to_return(status: 200, body: '{"data":{"id":"1"}}', headers: {})

  #   stub_request(:post, 'http://api.dubbletrack.test/api/v1/endpoint')
  #     .with(
  #       body: "{\"data\":{\"type\":\"track\",\"attributes\":{\"title\":\"Summer Haze\",\"artist_name\":\"Hollywood Gossip\",\"release_name\":\"Good Danny's Good Friends Vol. 1\",\"label_name\":\"Self Released\",\"played_at\":\"2019-10-31 20:40:35 -0500\"}}}",
  #       headers: {
  #         'Accept' => '*/*',
  #         'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
  #         'Content-Type' => 'application/json',
  #         'User-Agent' => 'Faraday v1.0.0',
  #         'X-Api-Key' => '_key_',
  #         'X-Api-Secret' => '_secret_'
  #       }
  #     )
  #     .to_return(status: 200, body: '{"data":{"id":"2"}}', headers: {})

  #   DubbleTrackRemote::CLI.start(["update"])
  # end
end
