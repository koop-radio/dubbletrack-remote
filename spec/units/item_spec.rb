# frozen_string_literal: true

require_relative '../../app/models/item';
require_relative '../spec_helper'

RSpec.describe "DubbleTrackRemote::Item", type: :model do
  describe 'track' do
    it 'formats date correctly' do
      title = 'Fine For Now'
      artist_name = 'Grizzly Bear'
      release_name = 'Veckatimest'
      label_name = 'Warp Records'
      item = DubbleTrackRemote::Item.new(title: title, artist_name: artist_name, release_name: release_name, label_name: label_name, played_at: Time.parse('2019-10-31 00:01:27'))

      expect(item.played_at.iso8601).to eq('2019-10-31T00:01:27-05:00')
      expect(item.track?).to eq(true)
      expect(item.traffic?).to eq(false)
    end

    it 'reports as invalid with blank values' do
      title = nil
      artist_name = nil
      release_name = 'Veckatimest'
      label_name = 'Warp Records'
      played_at = Time.parse('2019-10-31 00:01:27')

      item = DubbleTrackRemote::Item.new(title: title, artist_name: artist_name, release_name: release_name, label_name: label_name, played_at: played_at)

      expect(item.track?).to eq(false)
    end

    it 'reports as invalid with promo values' do
      title = 'This is a promo title'
      artist_name = nil
      release_name = nil
      label_name = nil
      played_at = Time.parse('2019-10-31 00:01:27')

      item = DubbleTrackRemote::Item.new(title: title, artist_name: artist_name, release_name: release_name, label_name: label_name, played_at: played_at)

      expect(item.track?).to eq(false)
      expect(item.traffic?).to eq(true)
    end
  end
end
